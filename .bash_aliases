# Aliases
alias ll='ls -l'
alias la='ls -A'
# git to manage dotfiles
alias dotconfig='git --git-dir $HOME/.dotfiles/.git/ --work-tree $HOME/.dotfiles'
