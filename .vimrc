set showcmd			" Show (partial) command in status line.
set showmatch		" Show matching brackets.
set ignorecase		" Do case insensitive matching
set incsearch		" Incremental search
set autowrite		" Automatically save before commands like :next and :make
syn on				" synthax hightlighting
set shiftwidth=2
set tabstop=2
set expandtab
" Abk�rzungen
" to write some email
ab mfG mit freundlichen Gr��en

" syntax file for txt2tags
au BufNewFile,BufRead *.t2t set ft=txt2tags

" save position in file
autocmd BufReadPost *
  \ if line("'\"") > 0 && line("'\"") <= line("$") |
  \   exe "normal g`\"" |
  \ endif

" saving view, aka openned and closed fold
set viewdir=~/.vim/view
au BufWinLeave *.tex mkview
au BufWinEnter *.tex silent loadview

