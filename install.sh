#!/bin/bash

# install bash aliases
if [ ! -f ~/.bash_aliases ]; then
  ln -s ~/.dotfiles/bash_aliases ~/.bash_aliases
else
  echo ".bash_aliases already there, skipping"
fi

# install vim files
if [ ! -f ~/.vimrc ]; then 
  ln -s ~/.dotfiles/vimrc ~/.vimrc
else
  echo ".vimrc already there, skipping"
fi
if [ ! -d ~/.vim ]; then
  ln -s ~/.dotfiles/vim ~/.vim
else
  echo ".vim already there, skipping"
fi 
